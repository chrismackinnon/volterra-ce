# **DEPRECATED**

This document has been deprecated. For the latest visit: [Create Kuberetes Site](https://volterra.io/docs/how-to/site-management/create-k8s-site)





# volterra-ce

To spawn a CE as a pod in an existing Kubernetes cluster, the following steps are required. The CE is spawned by creating a vpm and vpm will, in turn, spawn rest of Volterra services.

## prerequisities

1. Download kubectl with version of your k8s cluster. For example, if the cluster uses version 1.14.7.
- curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.7/bin/darwin/amd64/kubectl
- chmod +x ./kubectl
2. Fill up `ClusterName`, `Token`, `Latitude` and `Longitude` in `k8s/ce_k8s.yml` in `vpm-config` configmap
3. If the cluster uses Kubernetes version 1.13 or earlier, the following
steps are required before the vpm manifest (step 2 mentioned below) is appplied.

*  ssh to each cluster node and run the following
-  sudo sysctl -w "vm.nr_hugepages=386"
-  sudo systemctl restart kubelet

## install

1. Setup managed K8s cluster (required only if there is no pre-existing cluster):
- in Azure (AKS)
    1. az aks create --resource-group <group_name> --name <cluster_name> -s Standard_B4ms --node-count 2 --kubernetes-version 1.14.7
    2. az aks get-credentials --resource-group <group_name> --name <cluster_name> --file admin.conf
- in AWS (EKS)
    1. eksctl create cluster --name <cluster_name> --version 1.14 --region us-west-2 --nodegroup-name standard-workers --node-type t3.xlarge --nodes  2 --managed --ssh-access --kubeconfig admin.conf
- in GCP (GKE)
    1. export KUBECONFIG=admin.conf
    2. gcloud container clusters create <cluster name> --num-nodes=2 --machine-type=n1-standard-4 --node-version=1.14.10
- minikube (on a laptop)
    1. export KUBECONFIG=admin.conf
    2. minikube start --kubernetes-version v1.14.0 --driver=virtualbox --memory 8192
2. Apply vpm manifests:
- ./kubectl --kubeconfig=admin.conf apply -f k8s/ce_k8s.yml
3. Verify vpm (vpm will restart for the first time after it fetches new version):
- ./kubectl --kubeconfig=admin.conf get pod -n ves-system
- ./kubectl --kubeconfig=admin.conf logs statefulset/vp-manager -n ves-system -f
4. Approve registration

## scale up & down
1. Change number of replicas of vpm (there can run only VER per node)
- ./kubectl --kubeconfig=admin.conf edit statefulset/vp-manager -n ves-system

## decommision
1. Delete created resources
- ./kubectl --kubeconfig=admin.conf delete -f k8s/ce_k8s.yml
2. Cleanup in Volterra portal